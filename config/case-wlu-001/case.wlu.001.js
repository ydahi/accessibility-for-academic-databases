var retrofitName = "CASE.WLU.001";

console.log(retrofitName);

chrome.storage.sync.get(retrofitName, function(result) {
    allValues = Object.values(result);
    var status = allValues[0];
    var status_string = status.toString();
    if (status_string === "on") {
        console.log("Loading Case.WLU.001");
        retrofit_case_wlu_001();
    } else {
        console.log("Extension Error for Case.001");
    }
});

function retrofit_case_wlu_001() {
    console.log('Running retrofit case.001');

    $(".searchResult > .titleContainer > .title > .reference").each(function() {
        $(this).prepend(", &nbsp;");
        $(this).siblings(".name").children("a").append($(this));
    });    
}

$('.form-control').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        setTimeout(retrofit_case_wlu_001, 500);
    }
});