var retrofitName = "CASE.WLU.004";

console.log(retrofitName);

chrome.storage.sync.get(retrofitName, function(result) {
    allValues = Object.values(result);
    var status = allValues[0];
    var status_string = status.toString();
    if (status_string === "on") {
        console.log("Loading Case.WLU.004");
        retrofit_case_wlu_004();
    } else {
        console.log("Extension Error for Case.004");
    }
});

function retrofit_case_wlu_004() {
    console.log('Running retrofit case.004');

    


    
    // $("#mainContentWrapper").attr("aria-live","polite");

    $("#mainContentWrapper").on('DOMNodeInserted', function(e) {
        var searchTarget = $("#searchResults > .items > ul");
        if ( searchTarget.length) {
            searchTarget.attr("aria-live","polite");
            searchTarget.unbind('DOMSubtreeModified');
            searchTarget.one('DOMSubtreeModified', function(e) {
                // here is where we trigger the message
                // problem is that there are multiple changes to the dom tree
                // therefore this event is triggered multiple times.
                // need a way to only trigger it once so we can show 
                // a message that the search results were update only once
                // each time there is an update.

                if ( !$('#result-notification').length ){
                    searchTarget.prepend(`<p id="result-notification">there are results</p>`)
                } else {
                    console.log("already exists");
                }
                

            });
        }
    });

    function logit() {
        console.log("Search results updated");
    }

    // var searchTarget = document.getElementById("searchResults");
    // searchTarget.addEventListener ("DOMNodeInserted", function(ev) {
    //     if (ev.relatedNode == searchTarget) {
    //         alert("element inserted into div");
    //     }
    // });



 
}

// $( document ).ajaxComplete(function() {
//     console.log("ajax complete triggered");
// });
// if ($("#searchResults").length) {
//     console.log("found #searchResults");
// }

// $("#searchResults").bind('DOMNodeInserted', function(e) {
//     var element = e.target;
//     console.log("HERE" + element.length);
//     // setTimeout(function() {
//     //     $(element).fadeOut(1000, function() {
//     //         $(this).remove();
//     //     });
//     // }, 1000);
// });

// var observer = new MutationObserver(function(mutations) {
//     mutations.forEach(function(mutation) {
//         if (!mutation.addedNodes) return
//         for (var i = 0; i < mutation.addedNodes.length; i++) {
//             // do things to your newly added nodes here
//             var node = mutation.addedNodes[i]  
//             if ($(node).attr("id") == "searchResults") {
//                 console.log("WORKING");
//             }
//             if ($(node).hasClass("result")) {
//                 console.log("WORKING ==============");
//             }
//         }
//     })
// });
  
// observer.observe(document.body, {
//     childList: true,
//     subtree: true,
//     attributes: false,
//     characterData: false
// });