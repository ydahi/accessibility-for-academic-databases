var retrofitName = "CASE.WLU.002";

chrome.storage.sync.get(retrofitName, function(result) {
    allValues = Object.values(result);
    var status = allValues[0];
    var status_string = status.toString();
    if (status_string === "on") {
        console.log("Loading Case.WLU.002");
        retrofit_case_wlu_002();
    } else {
    }
});


function retrofit_case_wlu_002() {
    console.log('Running retrofit case.wlu.002');

    var wluSkipLink = `<a href="#wlu_a11y_content" class="skip-link">Skip to Laurier Library's barrier and workaround guide (press enter)</a>`;
    var wluBarrierInfoBlock = `
    <hr>
    <a name="AccessibilityHelp" id="wlu_a11y_content"></a>
    <div class="container">
        <h2 class="canlii">Accessibility, barriers, and workarounds by Laurier Library</h2>
        <h3>Known barriers</h3>
        <h4>Barrier: Search results</h4>
        <h5>Description</h5>
        <p>CanLII returns search results using an endless scrolling technique.</p>
        <p>If you use a screen reader to list all links, some result links may not be listed.  CanLII assumes a user will scroll to the bottom of the page with the mouse or keyboard. As the user scrolls down, CanLII automatically loads more results, if there are more.  When screen readers list all links, only the links currently on the page are listed. This give the impression there are a fixed number of results when there may be more.</p>
        <h6>Workaround 1</h5>
        <p>Use the page down key a few times to force a visual scroll down, then list the links. Repeat. This is not perfect.</p>
        <h5>Workaround 2</h5>
        <h3>Barrier in the way?</h2>
        <p>If a barrier is too much, please contact the Laurier Library's Accessibility Research Associate, Julie Schnurr <a href="mailto:jschnurr@wlu.ca">(jschnurr@wlu.ca)</a> for accommodation.</p>
    </div>
    <hr>
    `;

    $("body").prepend(wluSkipLink);
    $("#wrap").append(wluBarrierInfoBlock);

}
