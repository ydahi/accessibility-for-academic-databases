var retrofitName = "CASE.WLU.003";

console.log(retrofitName);

chrome.storage.sync.get(retrofitName, function(result) {
    allValues = Object.values(result);
    var status = allValues[0];
    var status_string = status.toString();
    if (status_string === "on") {
        console.log("Loading Case.WLU.003");
        retrofit_case_wlu_003();
    } else {
        console.log("Extension Error for Case.003");
    }
});

function retrofit_case_wlu_003() {
    console.log('Running retrofit case.003');

    $("span.resultCitationCount").each(function() {
        $(this).attr("tabindex","0");
    });    
}

$('.form-control').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        setTimeout(retrofit_case_wlu_003, 500);
    }
});