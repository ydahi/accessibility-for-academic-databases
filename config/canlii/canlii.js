// add label for form input elements
$("input.form-control.placeholder").each(function(){
    var placeholderText = $(this).attr("placeholder");
    var inputId = $(this).attr("id");
    var inputLabel = `<label for="${inputId}">${placeholderText}</label>`;
    if (placeholderText !== undefined) {
        $(this).parents(".inputTextZone").prepend(inputLabel);
    }
});

// add title to search button
$('button.search').attr('title','Search');

// add missing alt text to Canlii logo in header
$("#canliiLogo > a.canlii > img").attr("alt","CANLII Home");

// add aria-hidden=true for input fields used for autocomplete
$(".twitter-typeahead > .tt-hint").attr("aria-hidden", "true");

//
$("img#search-spinner").attr("aria-hidden", "true");

//
$("a.rssIconLink > i").attr("aria-hidden", "true").append('<span class="sr-only">RSS Feed</span>');;

//
$("img.flsc").attr("alt", "Federation of Law Societies of Canada Logo");
