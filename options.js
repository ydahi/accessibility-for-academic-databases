// Assign handlers immediately after making the request,
// and remember the jqxhr object for this request
var jqxhr = $.getJSON( "manifest.json", function() {
    console.log( "success" );
})
    .done(function(data) {
        console.log( "second success" );
        //console.log(data.content_scripts);
        $.each( data.content_scripts, function(i,option){
            //console.log(option.name);
            if (option.name) {
                template = `
                <div class="form-item">
                    <input id="option-${i}" type="checkbox" name="${option.name}" ${ option.enabled == "true" ? "checked": ""}>
                    <label for="option-${i}">${option.name}</label>
                    <p class="description">${option.description}</p>
                    <p>Default: ${option.enabled}</p>
                    <p>Author: ${option.author}</p>
                    <p>More info: <a href="${option.readmore}" alt="Read more about this retrofit. Opens in a new tab." target="_blank">${option.readmore}</a></p>
                </div>
                `;
                $('#options-list').append(template);
            }
            
        });
    })
    .fail(function() {
      console.log( "Error: could not load/parse manifest.json" );
    })
    .always(function() {
        restore_options();
    });

function save_options() {
    $("input[type=checkbox]").each(function(){
        var name = $(this).attr("name");
        var state;
        if ($(this).is(":checked")) {
            state = "on";
        } else {
            state = "off"
        }      
        chrome.storage.sync.set({
            [name]: state
        });
    });
    $(".messages").toggle().addClass("saved").html(`<p>Settings have been saved</p>`);
    setInterval(function(){ 
        $(".messages").css('display','none')
    }, 4000);
}

document.getElementById('save').addEventListener('click', save_options);
//document.getElementById('get').addEventListener('click', get_options);

function get_options() {
    chrome.storage.sync.get(null, function(items) {
        console.log(items);
    });
}

function restore_options() {
    $("input[type=checkbox]").each(function(){
        var name = $(this).attr("name");
        chrome.storage.sync.get(name, function(result) {
            allValues = Object.values(result);
            var status = allValues[0];
            if (status === "on") {
                $(`input[name="${name}"]`).prop("checked", true);
            }
        });   
    });
}