## Intro

Still in development.

## Get started

Follow these steps to install this chrome extension:

#### Step 1: Download this repo to your computer
* The download button is located beside the History, Find File, and Web IDE buttons just above and to the right.
* Download the repo as a zip file to your computer
* Unzip the file

#### Step 2: Chrome Extensions Developer Mode
* In your Chrome browser, go to chrome://extensions
* Turn on "Developer Mode" by toggling the toggle on the top-right of the page.
* Click on the "Load Unpacked" button and browse to the folder you unzipped the repo in
* Make sure the extension called "Accessible Academia 0.1" is enabled.

The extension is now installed and ready to be tested on the CANLII site.
